const mix = require('laravel-mix');
const path = require('path');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */
/**
 * webpack 配置
 */

mix.webpackConfig({
	output: {
		// 依据该路径进行编译以及异步加载
		publicPath: '/',
		// 注意开发期间不加 hash，以免自动刷新失败
		chunkFilename: `js/[name].js`
	},
	externals: [
		require('webpack-require-http'),
	],
});

mix.setPublicPath(path.normalize('public'));

mix.js('resources/js/xin/app.js', 'public/js/app.js');
mix.js('resources/js/index/index.js', 'public/js/index.js');
mix.js('resources/js/admin/index.js', 'public/js/admin.js');

mix.sass('resources/sass/index/app.scss', 'public/css/index');
mix.sass('resources/sass/admin/app.scss', 'public/css/admin');
mix.sass('resources/sass/admin/login.scss', 'public/css/admin');
