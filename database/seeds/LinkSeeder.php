<?php

use think\migration\Seeder;

class LinkSeeder extends Seeder{

	use \Xin\Thinkphp5\Database\Seeder;

	public function getDependencies(){
		return [
		];
	}

	/**
	 * Run Method.
	 * Write your database seeder using this method.
	 * More information on writing seeders is available here:
	 * http://docs.phinx.org/en/latest/seeding.html
	 */
	public function run(){
		$this->table('link')->insert([
			[
				'title'       => '刘小晋啦',
				'status'      => 1,
				'url'         => 'http://liuxiaojinla.com',
				'sort'        => 0,
				'expire_time' => time() + 31536000,
				'create_time' => time(),
			],
		])->save();
	}
}
