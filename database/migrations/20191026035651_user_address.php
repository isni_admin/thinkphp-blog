<?php

use Phinx\Db\Adapter\MysqlAdapter;
use think\migration\Migrator;

class UserAddress extends Migrator{

	/**
	 * Change Method.
	 * Write your reversible migrations using this method.
	 * More information on writing migrations is available here:
	 * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
	 * The following commands can be used in this method and Phinx will
	 * automatically reverse them when rolling back:
	 *    createTable
	 *    renameTable
	 *    addColumn
	 *    renameColumn
	 *    addIndex
	 *    addForeignKey
	 * Remember to call "create()" or "update()" and NOT "save()" when working
	 * with the Table class.
	 */
	public function change(){
		if($this->hasTable('user_address')){
			//			$this->dropTable('user_address');
			return;
		}

		$table = $this->table('user_address', [
			'engine'    => 'InnoDB',
			'signed'    => false,
			'encoding'  => 'utf8mb4',
			'collation' => 'utf8mb4_general_ci',
			'comment'   => '用户地址表',
		]);

		$table->addColumn('uid', 'integer', [
			'limit'   => 11,
			'signed'  => false,
			'comment' => '用户ID',
		])
			->addColumn('name', 'string', [
				'limit'   => 50,
				'default' => '',
				'comment' => '收货人姓名',
			])
			->addColumn('mobile', 'string', [
				'limit'   => 15,
				'default' => '1',
				'comment' => '收货人手机号',
			])
			->addColumn('country', 'string', [
				'limit'   => 30,
				'default' => '',
				'comment' => '国家',
			])
			->addColumn('province', 'string', [
				'limit'   => 30,
				'default' => '',
				'comment' => '省',
			])
			->addColumn('city', 'string', [
				'limit'   => 30,
				'default' => '',
				'comment' => '城市',
			])
			->addColumn('is_default', 'boolean', [
				'limit'   => MysqlAdapter::INT_TINY,
				'signed'  => false,
				'default' => 0,
				'comment' => '是否是默认地址',
			])
			->addColumn('update_time', 'integer', [
				'limit'   => 11,
				'signed'  => false,
				'default' => 0,
				'comment' => '更新时间',
			])
			->addColumn('create_time', 'integer', [
				'limit'   => 11,
				'signed'  => false,
				'default' => 0,
				'comment' => '创建时间',
			])
			->addIndex(['uid'])
			->create();
	}
}
