<?php

use Phinx\Db\Adapter\MysqlAdapter;
use think\migration\Migrator;

class Post extends Migrator{

	/**
	 * Change Method.
	 * Write your reversible migrations using this method.
	 * More information on writing migrations is available here:
	 * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
	 * The following commands can be used in this method and Phinx will
	 * automatically reverse them when rolling back:
	 *    createTable
	 *    renameTable
	 *    addColumn
	 *    renameColumn
	 *    addIndex
	 *    addForeignKey
	 * Remember to call "create()" or "update()" and NOT "save()" when working
	 * with the Table class.
	 */
	public function change(){
		// create the table
		if($this->hasTable('post')){
			//			$this->dropTable('post');
			return;
		}

		$table = $this->table('post', [
			'engine'    => 'InnoDB',
			'signed'    => false,
			'encoding'  => 'utf8mb4',
			'collation' => 'utf8mb4_general_ci',
			'comment'   => '文章表',
		]);

		$table->addColumn('uid', 'integer', [
			'limit'   => 11,
			'signed'  => false,
			'comment' => '用户ID',
		])
			->addColumn('title', 'string', [
				'limit'   => 48,
				'default' => '',
				'comment' => '标题',
			])
			->addColumn('category_id', 'integer', [
				'limit'   => 11,
				'signed'  => false,
				'default' => 0,
				'comment' => '所属分类',
			])
			->addColumn('badge_id_list', 'string', [
				'limit'   => 255,
				'default' => '',
				'comment' => '标签列表',
			])
			->addColumn('cover', 'string', [
				'limit'   => 128,
				'default' => '',
				'comment' => '封面',
			])
			->addColumn('content', 'text', [
				'comment' => '正文',
			])
			->addColumn('position', 'string', [
				'limit'   => 128,
				'default' => '',
				'comment' => '显示位置',
			])
			->addColumn('status', 'boolean', [
				'limit'   => MysqlAdapter::INT_TINY,
				'signed'  => false,
				'default' => 0,
				'comment' => '状态',
			])
			->addColumn('allow_comment', 'boolean', [
				'limit'   => MysqlAdapter::INT_TINY,
				'signed'  => false,
				'default' => 1,
				'comment' => '是否允许评论',
			])
			->addColumn('is_original', 'boolean', [
				'limit'   => MysqlAdapter::INT_TINY,
				'signed'  => false,
				'default' => 0,
				'comment' => '是否是原创',
			])
			->addColumn('original_url', 'string', [
				'limit'   => 255,
				'default' => '',
				'comment' => '原地址',
			])
			->addColumn('view_count', 'integer', [
				'limit'   => 11,
				'signed'  => false,
				'default' => 0,
				'comment' => '访问数量',
			])
			->addColumn('collect_count', 'integer', [
				'limit'   => 11,
				'signed'  => false,
				'default' => 0,
				'comment' => '点赞数量',
			])
			->addColumn('comment_count', 'integer', [
				'limit'   => 11,
				'signed'  => false,
				'default' => 0,
				'comment' => '评论数量',
			])
			->addColumn('share_count', 'integer', [
				'limit'   => 11,
				'signed'  => false,
				'default' => 0,
				'comment' => '转发数量',
			])
			->addColumn('good_time', 'integer', [
				'limit'   => 11,
				'signed'  => false,
				'default' => 0,
				'comment' => '精选时间',
			])
			->addColumn('top_time', 'integer', [
				'limit'   => 11,
				'signed'  => false,
				'default' => 0,
				'comment' => '置顶时间',
			])
			->addColumn('last_reply_uid', 'integer', [
				'limit'   => 11,
				'signed'  => false,
				'default' => 0,
				'comment' => '最后回复人',
			])
			->addColumn('last_reply_time', 'integer', [
				'limit'   => 11,
				'signed'  => false,
				'default' => 0,
				'comment' => '最后回复时间',
			])
			->addColumn('delete_time', 'integer', [
				'limit'   => 11,
				'signed'  => false,
				'default' => 0,
				'comment' => '删除时间',
			])
			->addColumn('publish_time', 'integer', [
				'limit'   => 11,
				'signed'  => false,
				'default' => 0,
				'comment' => '发布时间',
			])
			->addColumn('update_time', 'integer', [
				'limit'   => 11,
				'signed'  => false,
				'default' => 0,
				'comment' => '更新时间',
			])
			->addColumn('create_time', 'integer', [
				'limit'   => 11,
				'signed'  => false,
				'default' => 0,
				'comment' => '创建时间',
			])
			->addIndex(['category_id', 'uid'], [])
			->create();
	}

}
