<?php

use Phinx\Db\Adapter\MysqlAdapter;
use think\migration\Migrator;

class Setting extends Migrator{

	/**
	 * Change Method.
	 * Write your reversible migrations using this method.
	 * More information on writing migrations is available here:
	 * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
	 * The following commands can be used in this method and Phinx will
	 * automatically reverse them when rolling back:
	 *    createTable
	 *    renameTable
	 *    addColumn
	 *    renameColumn
	 *    addIndex
	 *    addForeignKey
	 * Remember to call "create()" or "update()" and NOT "save()" when working
	 * with the Table class.
	 */
	public function change(){
		if($this->hasTable('setting')){
			//			$this->dropTable('setting');
			return;
		}

		$table = $this->table('setting', [
			'engine'    => 'InnoDB',
			'signed'    => false,
			'encoding'  => 'utf8mb4',
			'collation' => 'utf8mb4_general_ci',
			'comment'   => '网站全局配置表',
		]);

		$table->addColumn('title', 'string', [
			'limit'   => 48,
			'default' => '',
			'comment' => '标题',
		])
			->addColumn('name', 'string', [
				'limit'   => 48,
				'default' => '',
				'comment' => '标识',
			])
			->addColumn('value', 'text', [
				'comment' => '配置值',
			])
			->addColumn('extra', 'string', [
				'limit'   => 255,
				'default' => '',
				'comment' => '扩展描述',
			])
			->addColumn('remark', 'string', [
				'limit'   => 255,
				'default' => '',
				'comment' => '描述',
			])
			->addColumn('status', 'boolean', [
				'limit'   => MysqlAdapter::INT_TINY,
				'signed'  => false,
				'default' => 0,
				'comment' => '状态',
			])
			->addColumn('type', 'integer', [
				'limit'   => MysqlAdapter::INT_TINY,
				'signed'  => false,
				'default' => 0,
				'comment' => '配置类型',
			])
			->addColumn('group', 'integer', [
				'limit'   => MysqlAdapter::INT_TINY,
				'signed'  => false,
				'default' => 0,
				'comment' => '分组',
			])
			->addColumn('sort', 'integer', [
				'limit'   => MysqlAdapter::INT_SMALL,
				'signed'  => false,
				'default' => 0,
				'comment' => '排序',
			])
			->addIndex(['name'], ['unique' => true])
			->create();

		$this->initData($table);
	}

	/**
	 * @param \think\migration\db\Table $table
	 */
	private function initData($table){
		$table->insert([
			[
				'title'  => '网站标题',
				'status' => 1,
				'type'   => 1,
				'group'  => 1,
				'name'   => 'site_title',
				'value'  => '刘小晋啦的博客',
				'extra'  => '',
				'remark' => '网站标题',
				'sort'   => 0,
			],
			[
				'title'  => '网站描述',
				'status' => 1,
				'type'   => 2,
				'group'  => 1,
				'name'   => 'site_description',
				'value'  => '',
				'extra'  => '',
				'remark' => '网站搜索引擎描述',
				'sort'   => 2,
			],
			[
				'title'  => '网站关键字',
				'status' => 1,
				'type'   => 2,
				'group'  => 1,
				'name'   => 'site_keywords',
				'value'  => 'PHP,Javascript,js,html,css,h5',
				'extra'  => '',
				'remark' => '网站搜索引擎关键字',
				'sort'   => 1,
			],
			[
				'title'  => '关闭站点',
				'status' => 1,
				'type'   => 4,
				'group'  => 1,
				'name'   => 'site_close',
				'value'  => '0',
				'extra'  => '0:关闭,1:开启',
				'remark' => '站点关闭后其他用户不能访问，管理员可以正常访问',
				'sort'   => 5,
			],
			[
				'title'  => '配置类型列表',
				'status' => 1,
				'type'   => 3,
				'group'  => 1,
				'name'   => 'config_type_list',
				'value'  => '0:数字
1:字符
2:文本
3:数组
4:枚举
5:开关
6:IPV4
7:电话
8:RMD
9:日期',
				'extra'  => '',
				'remark' => '主要用于数据解析和页面表单的生成',
				'sort'   => 7,
			],
			[
				'title'  => '配置分组',
				'status' => 1,
				'type'   => 3,
				'group'  => 1,
				'name'   => 'config_group_list',
				'value'  => '1:基本
2:内容
3:系统
4:微信',
				'extra'  => '',
				'remark' => '配置分组',
				'sort'   => 6,
			],
			[
				'title'  => '文档推荐位',
				'status' => 1,
				'type'   => 3,
				'group'  => 2,
				'name'   => 'document_position',
				'value'  => '1:列表推荐
2:频道推荐
4:首页推荐',
				'extra'  => '',
				'remark' => '文档推荐位，推荐到多个位置KEY值相加即可',
				'sort'   => 1,
			],
			[
				'title'  => '文档可见性',
				'status' => 1,
				'type'   => 3,
				'group'  => 2,
				'name'   => 'document_display',
				'value'  => '0:所有人可见
1:仅注册会员可见
2:仅管理员可见',
				'extra'  => '',
				'remark' => '文章可见性仅影响前台显示，后台不收影响',
				'sort'   => 2,
			],
			[
				'title'  => '允许用户注册',
				'status' => 1,
				'type'   => 4,
				'group'  => 1,
				'name'   => 'user_allow_register',
				'value'  => '1',
				'extra'  => '0:关闭
1:允许',
				'remark' => '是否开放用户注册',
				'sort'   => 8,
			],
			[
				'title'  => '开发者模式',
				'status' => 1,
				'type'   => 4,
				'group'  => 3,
				'name'   => 'devlop_mode',
				'value'  => '1',
				'extra'  => '0:关闭
1:开启',
				'remark' => '是否开启开发者模式',
				'sort'   => 0,
			],
			[
				'title'  => '不受限操作',
				'status' => 1,
				'type'   => 3,
				'group'  => 3,
				'name'   => 'allow_visit_list',
				'value'  => '0:admin/captcha/admin
1:admin/index/login
2:admin/index/logout
3:admin/error/index',
				'extra'  => '',
				'remark' => '',
				'sort'   => 3,
			],
			[
				'title'  => '仅超管可访问操作',
				'status' => 1,
				'type'   => 3,
				'group'  => 3,
				'name'   => 'deny_visit_list',
				'value'  => '0:admin/menu/index
1:admin/role/index
2:admin/role/access
3:admin/role/user',
				'extra'  => '',
				'remark' => '仅超级管理员可访问的控制器方法',
				'sort'   => 5,
			],
			[
				'title'  => '回复列表每页条数',
				'status' => 1,
				'type'   => 0,
				'group'  => 2,
				'name'   => 'reply_list_rows',
				'value'  => '20',
				'extra'  => '',
				'remark' => '',
				'sort'   => 0,
			],
			[
				'title'  => '后台允许访问IP',
				'status' => 1,
				'type'   => 2,
				'group'  => 3,
				'name'   => 'admin_allow_ip_list',
				'value'  => '',
				'extra'  => '',
				'remark' => '多个用逗号分隔，如果不配置表示不限制IP访问',
				'sort'   => 6,
			],
			[
				'title'  => '登录后不受限操作',
				'status' => 1,
				'type'   => 3,
				'group'  => 3,
				'name'   => 'allow_login_visit',
				'value'  => '0:admin/index/modify
1:admin/index/modifyPwd',
				'extra'  => '',
				'remark' => '',
				'sort'   => 4,
			],
		])->save();
	}
}
