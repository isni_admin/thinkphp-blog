<?php

use Phinx\Db\Adapter\MysqlAdapter;
use think\migration\Migrator;

class Category extends Migrator{

	/**
	 * Change Method.
	 * Write your reversible migrations using this method.
	 * More information on writing migrations is available here:
	 * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
	 * The following commands can be used in this method and Phinx will
	 * automatically reverse them when rolling back:
	 *    createTable
	 *    renameTable
	 *    addColumn
	 *    renameColumn
	 *    addIndex
	 *    addForeignKey
	 * Remember to call "create()" or "update()" and NOT "save()" when working
	 * with the Table class.
	 */
	public function change(){
		if($this->hasTable('category')){
			//			$this->dropTable('category');
			return;
		}

		$table = $this->table('category', [
			'engine'    => 'InnoDB',
			'signed'    => false,
			'encoding'  => 'utf8mb4',
			'collation' => 'utf8mb4_general_ci',
			'comment'   => '分类表',
		]);

		$table->addColumn('title', 'string', [
			'limit'   => 48,
			'default' => '',
			'comment' => '标题',
		])
			->addColumn('pid', 'integer', [
				'limit'   => 11,
				'signed'  => false,
				'default' => 0,
				'comment' => '父级ID',
			])
			->addColumn('description', 'string', [
				'limit'   => 255,
				'default' => '',
				'comment' => '描述(SEO优化)',
			])
			->addColumn('cover', 'string', [
				'limit'   => 128,
				'default' => '',
				'comment' => '封面',
			])
			->addColumn('keywords', 'string', [
				'limit'   => 255,
				'default' => '',
				'comment' => '关键字(SEO优化)',
			])
			->addColumn('sort', 'integer', [
				'limit'   => MysqlAdapter::INT_SMALL,
				'signed'  => false,
				'default' => 0,
				'comment' => '排序',
			])
			->addColumn('status', 'boolean', [
				'limit'   => MysqlAdapter::INT_TINY,
				'signed'  => false,
				'default' => 0,
				'comment' => '状态',
			])
			->addColumn('collect_count', 'integer', [
				'limit'   => 11,
				'signed'  => false,
				'default' => 0,
				'comment' => '点赞数量',
			])
			->addColumn('view_count', 'integer', [
				'limit'   => 11,
				'signed'  => false,
				'default' => 0,
				'comment' => '流量数量',
			])
			->addColumn('comment_count', 'integer', [
				'limit'   => 11,
				'signed'  => false,
				'default' => 0,
				'comment' => '评论数量',
			])
			->addColumn('good_time', 'integer', [
				'limit'   => 11,
				'signed'  => false,
				'default' => 0,
				'comment' => '精选时间',
			])
			->create();

		$this->initData($table);
	}

	/**
	 * @param \think\migration\db\Table $table
	 */
	private function initData($table){
		$table->insert([
			[
				'pid'         => 0,
				'title'       => '前端',
				'description' => '',
				'keywords'    => '',
				'sort'        => 0,
			],
			[
				'pid'         => 0,
				'title'       => '后端',
				'description' => '',
				'keywords'    => '',
				'sort'        => 0,
			],
			[
				'pid'         => 0,
				'title'       => '开源推荐',
				'description' => '',
				'keywords'    => '',
				'sort'        => 0,
			],
			[
				'pid'         => 0,
				'title'       => '易筋经',
				'description' => '',
				'keywords'    => '',
				'sort'        => 0,
			],
			[
				'pid'         => 1,
				'title'       => 'Javascript',
				'description' => '',
				'keywords'    => '',
				'sort'        => 0,
			],
			[
				'pid'         => 1,
				'title'       => 'Vue',
				'description' => '',
				'keywords'    => '',
				'sort'        => 0,
			],
			[
				'pid'         => 1,
				'title'       => 'ES6',
				'description' => '',
				'keywords'    => '',
				'sort'        => 0,
			],
			[
				'pid'         => 1,
				'title'       => '地图',
				'description' => '',
				'keywords'    => '',
				'sort'        => 0,
			],
			[
				'pid'         => 1,
				'title'       => '富文本',
				'description' => '',
				'keywords'    => '',
				'sort'        => 0,
			],
			[
				'pid'         => 1,
				'title'       => '文件上传',
				'description' => '',
				'keywords'    => '',
				'sort'        => 0,
			],
			[
				'title'       => 'PHP',
				'description' => '',
				'keywords'    => '',
				'sort'        => 0,
			],
			[
				'pid'         => 2,
				'title'       => 'NodeJs',
				'description' => '',
				'keywords'    => '',
				'sort'        => 0,
			],
		])->save();
	}
}
