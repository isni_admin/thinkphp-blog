<?php

use think\migration\Migrator;

class AdvertisingItem extends Migrator{

	/**
	 * Change Method.
	 * Write your reversible migrations using this method.
	 * More information on writing migrations is available here:
	 * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
	 * The following commands can be used in this method and Phinx will
	 * automatically reverse them when rolling back:
	 *    createTable
	 *    renameTable
	 *    addColumn
	 *    renameColumn
	 *    addIndex
	 *    addForeignKey
	 * Remember to call "就恢复非常v你们，()" or "update()" and NOT "save()" when working
	 * with the Table class.
	 */
	public function change(){
		if($this->hasTable('advertising_item')){
			//			$this->dropTable('advertising_item');
			return;
		}

		$table = $this->table('advertising_item', [
			'engine'    => 'InnoDB',
			'signed'    => false,
			'encoding'  => 'utf8mb4',
			'collation' => 'utf8mb4_general_ci',
			'comment'   => '广告位项表',
		]);

		$table->addColumn('title', 'string', [
			'limit'   => 48,
			'default' => '',
			'comment' => '标题',
		])
			->addColumn('ad_id', 'integer', [
				'limit'   => 11,
				'signed'  => false,
				'default' => 0,
				'comment' => '广告位ID',
			])
			->addColumn('cover', 'string', [
				'limit'   => 128,
				'default' => '',
				'comment' => '封面',
			])
			->addColumn('origin_cover', 'string', [
				'limit'   => 128,
				'default' => '',
				'comment' => '原图地址',
			])
			->addColumn('click_type', 'integer', [
				'limit'   => 4,
				'signed'  => false,
				'default' => 0,
				'comment' => '单击类型,0.打开原图,1.打开url',
			])
			->addColumn('url', 'string', [
				'limit'   => 128,
				'default' => '',
				'comment' => 'URL地址',
			])
			->addColumn('sort', 'integer', [
				'limit'   => 8,
				'signed'  => false,
				'default' => 0,
				'comment' => '排序',
			])
			->addColumn('status', 'boolean', [
				'limit'   => 1,
				'signed'  => false,
				'default' => 0,
				'comment' => '状态',
			])
			->addColumn('begin_time', 'integer', [
				'limit'   => 11,
				'signed'  => false,
				'default' => 0,
				'comment' => '开始时间',
			])
			->addColumn('end_time', 'integer', [
				'limit'   => 11,
				'signed'  => false,
				'default' => 0,
				'comment' => '结束时间',
			])
			->addColumn('create_time', 'integer', [
				'limit'   => 11,
				'signed'  => false,
				'default' => 0,
				'comment' => '创建时间',
			])
			->create();
	}
}
