import "../plugins/layui";

const SHOW_CLASS = 'layui-show',
	HIDE_CLASS = 'layui-hide',
	THIS_CLASS = 'layui-this',
	DISABLED_CLASS = 'layui-disabled',
	TEMP_CLASS = 'template',
	APP_BODY_ELEM_ID = '#LAY_app_body',
	APP_FLEXIBLE_ELEM_ID = 'LAY_app_flexible',
	FILTER_TAB_TBAS_CLASS = 'layadmin-layout-tabs',
	APP_SPREAD_SM_CLASS = 'layadmin-side-spread-sm',
	TABS_BODY = 'layadmin-tabsbody-item',
	ICON_SHRINK_CLASS = 'layui-icon-shrink-right',
	ICON_SPREAD_CLASS = 'layui-icon-spread-left',
	SIDE_SHRINK_CLASS = 'layadmin-side-shrink',
	SIDE_MENU_ELEM_ID = 'LAY-system-side-menu';

// 收缩/弹开
xin.sideFlexible = function(status) {
	const iconElem = $('#' + APP_FLEXIBLE_ELEM_ID),
		screen = xin.getScreenType(),
		app = $(".layadmin-tabspage-none");

	//设置状态，PC：默认展开,移动：默认收缩
	if (status === 'spread') {
		//切换到展开状态的 icon，箭头：←
		iconElem.removeClass(ICON_SPREAD_CLASS).addClass(ICON_SHRINK_CLASS);

		//移动：从左到右位移；PC：清除多余选择器恢复默认
		if (screen < 2) {
			app.addClass(APP_SPREAD_SM_CLASS);
		} else {
			app.removeClass(APP_SPREAD_SM_CLASS);
		}

		app.removeClass(SIDE_SHRINK_CLASS)
	} else {
		//切换到搜索状态的 icon，箭头：→
		iconElem.removeClass(ICON_SHRINK_CLASS).addClass(ICON_SPREAD_CLASS);

		//移动：清除多余选择器恢复默认；PC：从右往左收缩
		if (screen < 2) {
			app.removeClass(SIDE_SHRINK_CLASS);
		} else {
			app.addClass(SIDE_SHRINK_CLASS);
		}

		app.removeClass(APP_SPREAD_SM_CLASS)
	}

	layui.event.call(this, 'admin', 'side({*})', {
		status: status
	});
}

// 事件器
const events = (function() {
	return {
		// 伸缩
		flexible(othis) {
			const iconElem = othis.find('#' + APP_FLEXIBLE_ELEM_ID),
				isSpread = iconElem.hasClass(ICON_SPREAD_CLASS),
				status = isSpread ? 'spread' : null;
			xin.sideFlexible(status);
		},

		//全屏
		fullscreen(othis) {
			const SCREEN_FULL = 'layui-icon-screen-full'
				, SCREEN_REST = 'layui-icon-screen-restore'
				, iconElem = othis.children("i"),
				ELEM = iconElem.hasClass(SCREEN_FULL) ? document.body : document;

			if (iconElem.hasClass(SCREEN_FULL)) {
				if (ELEM.webkitRequestFullScreen) {
					ELEM.webkitRequestFullScreen();
				} else if (ELEM.mozRequestFullScreen) {
					ELEM.mozRequestFullScreen();
				} else if (ELEM.requestFullScreen) {
					ELEM.requestFullscreen();
				}
				iconElem.addClass(SCREEN_REST).removeClass(SCREEN_FULL);
			} else {
				if (ELEM.webkitCancelFullScreen) {
					ELEM.webkitCancelFullScreen();
				} else if (ELEM.mozCancelFullScreen) {
					ELEM.mozCancelFullScreen();
				} else if (ELEM.cancelFullScreen) {
					ELEM.cancelFullScreen();
				} else if (ELEM.exitFullscreen) {
					ELEM.exitFullscreen();
				}

				iconElem.addClass(SCREEN_FULL).removeClass(SCREEN_REST);
			}
		},

		// 遮罩层
		shade() {
			xin.sideFlexible();
		},

		// 退出登录
		logout() {
			window.location.href = '/admin/login/logout';
		}
	};
})();

$(function() {
	const container = $(".layadmin-tabspage-none");

	// 初始化事件操作
	$("[layadmin-event]").on('click', function() {
		const othis = $(this), eventName = othis.attr('layadmin-event');
		if (!events[eventName]) {
			console.warn(`未实现的事件名称${eventName}`);
			return;
		}

		try {
			events[eventName].call(this, othis)
		} catch (e) {
			console.warn()
		}
	});

	// 初始化屏幕
	(function() {
		const toggle = function() {
			const status = xin.getScreenType() < 2 ? null : 'spread';
			xin.sideFlexible(status);
		};
		toggle();

		let resizeTimer;
		$(window).resize(function() {
			clearTimeout(resizeTimer);
			resizeTimer = setTimeout(toggle, 250);
		});
	})();

	//监听侧边导航点击事件
	layui.element.on('nav(layadmin-system-side-menu)', function(elem) {
		if (elem.siblings('.layui-nav-child')[0] && container.hasClass(SIDE_SHRINK_CLASS)) {
			xin.sideFlexible('spread');
		}
	});
});
