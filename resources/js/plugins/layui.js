layui.config({
	base: '/vendor/layui-modules/',
	version: '1.0.0'
}).extend({
	formSelects: 'formSelects/formSelects-v4',
	echarts: 'echarts/echarts',
	echartsTheme: 'echarts/echartsTheme',
});
