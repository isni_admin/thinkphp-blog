const webpackRequireHttp = require('webpack-require-http');
const customImport = webpackRequireHttp.custom({
	rules: function(filepath, request) {
		// console.log(filepath, request);
		return request;
	}
});

/**
 * 动态加载js
 * @param {string} path
 * @return Promise
 */
export default function dynamicImport(path) {
	const key = dynamicImport.resolve(path);

	if (!dynamicImport.promises[key]) {
		dynamicImport.promises[key] = customImport('', path, function(err, content) {
			return eval(content);
		});
	}
	return dynamicImport.promises[key];
}

// 全局缓存列表
dynamicImport.promises = {};

// 生成全局缓存key
dynamicImport.resolve = function(path) {
	return btoa(path);
};
