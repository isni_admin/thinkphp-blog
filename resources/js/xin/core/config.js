import {assign} from './base';

const globalConfig = {};

/**
 * 设置config
 * @param {*} config
 */
export function setConfig(config) {
	return assign(globalConfig, config);
}

/**
 * 获取配置信息
 * @param {string} key
 * @param defaultValue
 * @return {*}
 */
export function getConfig(key, defaultValue = null) {
	return globalConfig[key] !== undefined && globalConfig[key] !== null
		? globalConfig[key] : defaultValue;
}
