export class CustomError extends Error{
	constructor(message) {
		super(message);
		this.name = "CustomError";
	}
}

export class InvalidParamsError extends CustomError {
	constructor(message) {
		super(message);
	}
}
