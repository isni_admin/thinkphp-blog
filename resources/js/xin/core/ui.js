/**
 * 获取 dataset 数据并转换为对象
 * @param {*} target
 * @param {String} name
 * @param {*} [defaultValue]
 * @return {*}
 */
export function getDataOptions(target, name, defaultValue = {}) {
	target = $(target);

	const value = target.data(name);

	if (!value) {
		return defaultValue;
	}

	if (typeof value === 'object') {
		return value;
	}

	try {
		return eval("(" + value + ")");
	} catch (e) {
	}

	return defaultValue;
}

/**
 * 获取组件名
 * @param {*} target
 * @param {String,Array} names
 * @return {String}
 */
export function getControlName(target, names) {
	target = $(target);

	names = typeof names === "object" ? names : names.split(",");
	for (let i = 0; i < names.length; i++) {
		if (undefined !== target.data(names[i])) {
			return names[i];
		}
	}

	return null;
}

/**
 * 设置表单的值
 * @param {String} name
 * @param {*} value
 */
export function setValue(name, value) {
	let first = name.substr(0, 1), input, i = 0, val;
	if (value === "") return;
	if ("#" === first || "." === first) {
		input = $(name);
	} else {
		input = $("[name='" + name + "']");
	}

	if (input.eq(0).is(":radio")) { //单选按钮
		input.filter("[value='" + value + "']").each(function() {
			this.checked = true
		});
	} else if (input.eq(0).is(":checkbox")) { //复选框
		if (!$.isArray(value)) {
			val = [];
			val[0] = value;
		} else {
			val = value;
		}
		for (i = 0; i < val.length; i++) {
			input.filter("[value='" + val[i] + "']").each(function() {
				this.checked = true
			});
		}
	} else {  //其他表单选项直接设置值
		input.val(value);
	}
}

/**
 * 设置是否选中
 * @param {String} name
 * @param {*} checked
 */
export function setChecked(name, checked) {
	$(name).prop('checked', isNaN(checked) ? checked : parseInt(checked));
}

/**
 * 插入值并获得焦点
 * @param {*} obj
 * @param {string} str
 */
export function focusInsert(obj, str) {
	let result, val = obj.value;
	obj.focus();
	if (document.selection) { //ie
		result = document.selection.createRange();
		document.selection.empty();
		result.text = str;
	} else {
		result = [val.substring(0, obj.selectionStart), str, val.substr(obj.selectionEnd)];
		obj.focus();
		obj.value = result.join('');
	}
}

/**
 * 表单验证
 * @param {jQuery,string,HTMLElement} target
 * @param {Object} [options]
 */
export function formValid(target, options) {
	$(target).valid(options);
}

/**
 * 是否有form验证器
 * @return {boolean}
 */
export function isSupportFormValidator() {
	return $.fn.valid;
}

/**
 * 屏幕类型
 * @return {number}
 */
export function getScreenType() {
	const width = $(window).width();
	if (width >= 1200) {
		return 3; //大屏幕
	} else if (width >= 992) {
		return 2; //中屏幕
	} else if (width >= 768) {
		return 1; //小屏幕
	} else {
		return 0; //超小屏幕
	}
}

/**
 * 全屏
 * @param {boolean} status
 */
export function fullscreen(status) {
	const ELEM = status ? document.body : document;

	if (status) {
		if (ELEM.webkitRequestFullScreen) {
			ELEM.webkitRequestFullScreen();
		} else if (elem.mozRequestFullScreen) {
			ELEM.mozRequestFullScreen();
		} else if (ELEM.requestFullScreen) {
			ELEM.requestFullscreen();
		}
	} else {
		if (ELEM.webkitCancelFullScreen) {
			ELEM.webkitCancelFullScreen();
		} else if (ELEM.mozCancelFullScreen) {
			ELEM.mozCancelFullScreen();
		} else if (ELEM.cancelFullScreen) {
			ELEM.cancelFullScreen();
		} else if (ELEM.exitFullscreen) {
			ELEM.exitFullscreen();
		}
	}
}

/**
 * 新标签页打开地址
 * @param {string} url
 */
export function openUrl(url) {
	const a = $('<a target="_blank"><span></span></a>');
	a.attr('href', url);
	a.css('display', 'none');
	a.click(function() {
		a.remove();
	});
	a.appendTo(document.body);
	a.find('span').trigger('click');
}
