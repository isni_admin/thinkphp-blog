/**
 * 格式化日期
 * @param {string} format
 * @param {Number|Date} time
 * @return {string}
 */
export default function date(format, time) {
	time = time || new Date();
	time = time instanceof Date ? time : new Date(time);
	const o = {
		"M+": time.getMonth() + 1,                 //月份
		"d+": time.getDate(),                    //日
		"h+": time.getHours(),                   //小时
		"m+": time.getMinutes(),                 //分
		"s+": time.getSeconds(),                 //秒
		"q+": Math.floor((time.getMonth() + 3) / 3), //季度
		"S": time.getMilliseconds()             //毫秒
	};

	if (/(y+)/.test(format)) {
		format = format.replace(RegExp.$1, (time.getFullYear() + "").substr(4 - RegExp.$1.length));
	}

	for (const k in o) {
		if (new RegExp("(" + k + ")").test(format)) {
			format = format.replace(RegExp.$1, (RegExp.$1.length === 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
		}
	}

	return format;
}

/**
 * 快速格式化日期
 * @param {Number|Date} [time]
 * @param {boolean} [isShowSeconds]
 */
date.datetime = function(time, isShowSeconds) {
	isShowSeconds = isShowSeconds || false;
	return date("yyyy-MM-dd hh:mm" + (isShowSeconds ? ":ss" : ""), time);
};

/**
 * 只格式化日期部分
 * @param {Number|Date} [time]
 */
date.date = function(time) {
	return date("yyyy-MM-dd", time);
};

/**
 * 只格式化时间部分
 * @param {Number|Date} [time]
 * @param {boolean} [isShowSeconds]
 */
date.time = function(time, isShowSeconds) {
	isShowSeconds = isShowSeconds || false;
	return date("hh:mm" + (isShowSeconds ? ":ss" : ""), time);
};
/**
 * 获取相对时间
 * @param timeStamp
 * @return {string}
 */
date.relative = function(timeStamp) {
	const currentTime = (new Date()).getTime();

	// 判断传入时间戳是否早于当前时间戳
	const IS_EARLY = timeStamp <= currentTime;
	// 获取两个时间戳差值
	let diff = currentTime - timeStamp;

	// 如果IS_EARLY为false则差值取反
	if (!IS_EARLY) {
		diff = -diff;
	}

	const dirStr = IS_EARLY ? '前' : '后';

	let resStr;

	if (diff < 1000) {
		resStr = '刚刚';
	} else if (diff < 60000) { // 少于等于59秒
		resStr = Math.floor(diff / 1000) + '秒' + dirStr;
	} else if (diff >= 60000 && diff < 3600000) { // 多于59秒，少于等于59分钟59秒
		resStr = Math.floor(diff / 60000) + '分钟' + dirStr;
	} else if (diff >= 3600000 && diff < 86400000) { // 多于59分钟59秒，少于等于23小时59分钟59秒
		resStr = Math.floor(diff / 3600000) + '小时' + dirStr;
	} else if (diff >= 86400000 && diff < 2623860000) { // 多于23小时59分钟59秒，少于等于29天59分钟59秒
		resStr = Math.floor(diff / 86400000) + '天' + dirStr;
	} else if (diff >= 2623860000 && diff <= 31567860000 && IS_EARLY) { // 多于29天59分钟59秒，少于364天23小时59分钟59秒，且传入的时间戳早于当前
		resStr = date('MM-dd hh:mm', timeStamp);
	} else {
		resStr = date.date(timeStamp);
	}

	return resStr;
};
