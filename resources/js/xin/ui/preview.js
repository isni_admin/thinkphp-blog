import {optimize} from "../core";

/**
 * 预览
 * @param {{
 * title?:string,
 * urls:Array<string>,
 * current?:string|number,
 * }|*} options
 * @return Promise
 */
export default function(options) {
	if (typeof options.current === "string") {
		for (let i = 0; i < options.urls.length; i++) {
			if (options.urls[i] === options.current) {
				options.current = i;
			}
		}
	}

	options = optimize({
		title: options.title,
		data: options.urls.map(url => ({src: url})),
		start: options.current
	});

	return new Promise((resolve) => {
		layui.layer.photos({
			photos: options,
			anim: options.anim || 5
		});
		resolve();
	});
};
