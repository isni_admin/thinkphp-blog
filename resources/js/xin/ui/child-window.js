/**
 * 打开一个页面
 * @param {{
 * url:string,
 * closed:function
 * }|*} options
 * @return Promise<{layero:jQuery,index:number,iframe:HTMLIFrameElement}>
 */
export default function(options) {
	const preWidth = $(window).width() * 0.8 + "px",
		preHeight = $(window).height() * 0.8 + "px";

	options = Object.assign({
		area: options.area ? options.area : [preWidth, preHeight],
		type: 2,
		title: '正在加载...',
		content: options.url,
		end: options.closed
	});

	return new Promise((resolve) => {
		options.success = (layero, index) => {
			const iFrame = layero.find("iframe");

			const result = {
				layero: layero,
				index: index,
				iframe: iFrame[0]
			};

			const updateTitle = function() {
				try{
					layer.title(iFrame[0].contentWindow.document.title, index);
				}catch (e) {
					layer.title("", index);
				}

			};

			if (iFrame[0].contentWindow) {
				updateTitle();
				resolve(result);
			} else {
				iFrame.on("load", () => {
					updateTitle();
					resolve(result);
				});
			}
		};
		layer.open(options);
	});
}
