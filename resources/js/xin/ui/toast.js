/**
 * Toast
 * @param {{
 * content:string,
 * time?:number,
 * close?:Function
 * }|string} options
 * @return Promise<*>
 */
export function showToast(options) {
	if (typeof options === 'string') {
		options = {content: options}
	}
	options = Object.assign({
		offset: '70px',
		anim: 1,
	}, options);

	const content = options.content;
	delete options.content;

	return new Promise(function(resolve) {
		layer.msg(content, options, resolve)
	});
}

/**
 * 提示成功
 * @param {string} title
 * @return Promise<*>
 */
export function hintSuccess(title) {
	return showToast({
		content: title,
		icon: 6
	})
}

/**
 * 提示失败
 * @param {string} title
 * @return Promise<*>
 */
export function hintError(title) {
	return showToast({
		content: title,
		icon: 0,
		anim: 6,
	})
}
