/**
 * 关闭窗体/层
 * @param {*} id
 */
export default function(id) {
	layer.close(id);
}
