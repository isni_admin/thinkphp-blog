const load = (function() {
	let promise = null;

	return function() {
		if (promise === null) {
			promise = import(/* webpackChunkName: "wangeditor" */'wangeditor');
		}
		return promise;
	}
})();


/**
 * 实例化一个编辑器
 * @param {{el:HTMLElement|string}} options
 */
export default function(options) {
	let container = $(options.el);
	let textarea;
	if ("TEXTAREA" === container[0].tagName) {
		textarea = container;
		container = $('<div></div>');
		textarea.before(container);
		container.html(textarea.val());
		container.addClass('editor');
	} else {
		textarea = $('<textarea></textarea>').after(container);
		textarea.val(container.html());
	}
	textarea.hide();

	if (options.name) {
		textarea.attr('name', options.name);
	}

	return new Promise(async function(resolve) {
		const WangEditor = (await load()).default;
		const editor = new WangEditor(container[0]);

		// 自定义菜单配置
		editor.customConfig.menus = defaultMenus();
		editor.customConfig.zIndex = 999;

		// 监控变化，同步更新到 textarea
		editor.customConfig.onchange = function(html) {
			textarea.val(html);
		}

		editor.create();

		// 更新样式
		updateStyle(editor);

		textarea.val(editor.txt.html());

		resolve({
			getContent() {
				return editor.txt.html();
			},
			setContent(text) {
				editor.txt.html(text);
				textarea.val(text);
			},
			ins: editor
		});
	});
};


// 更新样式
function updateStyle(editor) {
	console.log(editor)
	const toolbar = $(editor.$toolbarElem[0]);
	const textContainer = $(editor.$textContainerElem[0]);

	toolbar.css('background-color', 'white').css('border', 'none');
	textContainer.css('border', 'none');

	// 更新字号显示
	// const fontSizeTexts = ["超小号"];
	// editor.menus.menus.fontSize.droplist.opt.list.forEach(function(item, index) {
	// 	item.$elem.text(fontSizeTexts[index]);
	// 	console.log(item)
	// });
	// console.log(toolbar.children().eq(2));

}

// 默认菜单
function defaultMenus() {
	return [
		'head',  // 标题
		'bold',  // 粗体
		// 'fontSize',  // 字号
		// 'fontName',  // 字体
		'italic',  // 斜体
		'underline',  // 下划线
		'strikeThrough',  // 删除线
		'foreColor',  // 文字颜色
		// 'backColor',  // 背景颜色
		'link',  // 插入链接
		'list',  // 列表
		'justify',  // 对齐方式
		'quote',  // 引用
		// 'emoticon',  // 表情
		'image',  // 插入图片
		// 'table',  // 表格
		// 'video',  // 插入视频
		'code',  // 插入代码
		'undo',  // 撤销
		'redo'  // 重复
	]
}
