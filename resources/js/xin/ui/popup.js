import {assign} from "../core";

export default function(options) {
	options = assign({
		title: false,
		type: 1,
		area: [
			options.width || '420px',
			options.height || '280px'
		]
	}, options || {});

	// 转换与剔除不必要的字段
	options.end = options.closed;
	delete options.closed;

	return new Promise((resolve) => {
		options.success = function(layero, index) {
			resolve({
				id: index,
				popup: layero
			});
		};
		layer.open(options);
	});
};
