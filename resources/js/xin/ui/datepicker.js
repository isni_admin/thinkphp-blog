import {optimize} from "../core";

/**
 * DatePicker
 * @param {{
 *     el:HTMLElement,
 *     type?:string,
 *     range?:boolean,
 *     format?:string,
 *     value?:string,
 *     min?:string,
 *     max?:string,
 *     position?:string,
 *     mark?:Object,
 *     success?:function,
 * }|*} options
 * @return Promise
 */
export default function(options) {
	options = optimize({
		elem: options.el,
		type: options.type,
		range: options.range,
		format: options.format,
		value: options.value,
		min: options.min,
		max: options.max,
		position: options.position,
		calendar: true,
		mark: options.mark,
		trigger: 'click',
		done: options.success
	});

	return new Promise(function(resolve) {
		options.ready = resolve;
		layui.laydate.render(options);
	})
};
