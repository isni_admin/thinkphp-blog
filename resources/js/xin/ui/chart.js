export default function(options) {
	return new Promise(function(resolve) {
		layui.define('echarts', function() {
			const echarts = layui.echarts;

			const myCharts = echarts.init($(options.el)[0], layui.echartsTheme);
			myCharts.setOption(options);

			resolve(myCharts);
		})
	});
};
