import {optimize} from "../core";

/**
 * ColorPicker
 * @param {{
 *     el:HTMLElement,
 *     color?:string,
 *     format?:string,
 *     alpha?:boolean,
 *     colors?:Array<string>,
 *     success?:function,
 * }|*} options
 * @return Promise
 */
export default function(options) {
	options = optimize({
		elem: options.el,
		color: options.color,
		format: options.format,
		alpha: options.alpha,
		colors: options.colors,
		predefine: !!options.colors,
		done: options.success
	});

	return Promise.resolve(
		layui.colorpicker.render(options)
	);
};
