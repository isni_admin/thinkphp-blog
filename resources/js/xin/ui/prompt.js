/**
 * Prompt
 * @param {{
 * title?:string,
 * formType?:number,
 * value?:string,
 * maxlength?:number,
 * area?:Array,
 * thisArg?:*,
 * created?:Function,
 * }|*} options
 * @return Promise
 */
export default function(options) {
	// 转换与剔除不必要的字段
	options.success = options.created;
	options.end = options.closed;
	delete options.created;
	delete options.closed;

	return new Promise((resolve) => {
		layer.prompt(options, (value, index) => {
			resolve({value: value, index: index});

			if (options.autoClose !== false) {
				layer.close(index);
			}
		});
	});
}
