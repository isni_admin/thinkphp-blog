/**
 * 获取子目标jQuery对象
 * @param {*} self
 * @return {*}
 */
import {showModal} from "../ui";

function makeTargetJQuery(self) {
	let target = self.data("target");

	if (!target) {
		throw Error('请配置data-target属性');
	}

	return $(target);
}

/**
 * layui checkbox
 * @param target
 * @param childTarget
 */
function layuiAdapter(target, childTarget) {
	const formFilter = 'form_' + new Date().getTime();
	target.closest('.layui-form').attr('lay-filter', formFilter);
	layui.form.on('checkbox', function(data) {
		if (undefined !== $(data.elem).data('choiceCheck')) {
			childTarget.prop("checked", data.elem.checked);
		} else {
			let isCheck = true;
			childTarget.each((index, checkItem) => {
				if ($(checkItem).is(":checked") === false) {
					isCheck = false;
					return false;
				}
			});
			target.prop("checked", isCheck);
		}
		layui.form.render('checkbox', formFilter);
	});
}

/**
 * default checkbox
 * @param target
 * @param childTarget
 */
function defaultAdapter(target, childTarget) {
	target.on($.fn.iCheck ? "ifChanged" : "change", function() {
		childTarget.prop("checked", target.is(":checked"));
		$.fn.iCheck && childTarget.iCheck('update');
	});

	//if change someone checkbox,that all-checkbox-button update state.
	childTarget.on($.fn.iCheck ? "ifChanged" : "change", function() {
		let isCheck = true;
		childTarget.each((index, checkItem) => {
			if ($(checkItem).is(":checked") === false) {
				isCheck = false;
				return false;
			}
		});
		target.prop("checked", isCheck);
		$.fn.iCheck && target.iCheck('update');
	});
}

/**
 * 全选/取消全选
 * @param {*} target
 */
export function choiceCheckBox(target) {
	target = $(target);
	const childTarget = makeTargetJQuery(target);

	if (window.layui) { //layui
		layuiAdapter(target, childTarget);
	} else { //原始或iCheck
		defaultAdapter(target, childTarget);
	}
}

/**
 * 获取checkbox选中的值
 * @param {jQuery,string,HTMLElement} target
 */
export function getChoiceCheckBoxValues(target) {
	target = $(target);
	const childTarget = makeTargetJQuery(target);

	const values = [];
	childTarget.each((index, checkboxItem) => {
		checkboxItem = $(checkboxItem);
		checkboxItem.is(":checked") && values.push(checkboxItem.val());
	});

	return values;
}

/**
 * 获取checkbox没选中的值
 * @param {jQuery,string,HTMLElement} target
 */
export function getChoiceUnCheckBoxValues(target) {
	target = $(target);
	const childTarget = makeTargetJQuery(target);

	const values = [];
	childTarget.each((index, checkboxItem) => {
		checkboxItem = $(checkboxItem);
		!checkboxItem.is(":checked") && values.push(checkboxItem.val());
	});

	return values;
}

/**
 * 如果有选中的checkbox则返回数据
 * @param {jQuery,string,HTMLElement,Function} target
 * @param {Function} [callback]
 * @param {Object} [thisArg]
 */
export function choiceCheckBoxHas(target, callback, thisArg) {
	const values = this.getChoiceCheckBoxValues(target);
	if (values.length === 0) {
		showModal({
			icon: 'error',
			content: '亲，你未选择数据！'
		});
		return false;
	}

	callback.call(thisArg || this, values);
}
