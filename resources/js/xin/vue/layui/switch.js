// switch
window.Vue && Vue.component('layui-switch', {
	template: `
<div class="layui-unselect layui-form-switch" lay-skin="_switch" :class="classs" @click="onCheck">
	<em>{{showTitle}}</em><i></i>
</div>
`,
	model: {
		prop: 'checked',
		event: 'change'
	},
	props: {
		value: {
			type: Array,
			default: [false, true]
		},
		text: {
			type: String,
			default: '关闭|开启'
		},
		checked: null
	},
	computed: {
		showTitle: function() {
			var title = this.text.split('|', 2);
			return this.value[0] === this.checked ? title[0] : title[1];
		},
		classs: function() {
			return {
				'layui-form-onswitch': this.value[0] !== this.checked
			};
		}
	},
	methods: {
		onCheck: function() {
			this.$emit('change', this.value[0] === this.checked ? this.value[1] : this.value[0]);
		}
	}
});
