// checkbox
window.Vue && Vue.component('layui-checkbox', {
	template: `
		<div class="layui-unselect layui-form-checkbox" lay-skin="primary" :class="classs" @click="onCheck">
			<span><slot></slot></span>
			<i class="layui-icon layui-icon-ok"></i>
		</div>
	`,
	model: {
		prop: 'checked',
		event: 'change'
	},
	props: {
		value: {
			type: null,
			default: true
		},
		checked: null,
		disbaled: Boolean
	},
	computed: {
		isChecked: function() {
			if (this.checked !== undefined) {
				var values = typeof this.checked === 'object' && this.checked.length !== undefined ? this.checked : [];
				return values.indexOf(this.value) !== -1;
			} else {
				return this.checked;
			}
		},
		classs: function() {
			return {
				'layui-checkbox-disbaled': this.disbaled,
				'layui-disabled': this.disbaled,
				'layui-form-checked': this.isChecked
			};
		},
	},
	methods: {
		onCheck: function() {
			if (this.disbaled) return;

			if (typeof this.value === 'boolean') {
				this.checked = true;
				this.$emit('change', true);
			} else {
				var value = this.checked;
				if (typeof value !== 'object' || value.length === undefined) {
					value = [];
				}

				var index = value.indexOf(this.value);
				if (index === -1) {
					value.push(this.value);
				} else {
					value.splice(index, 1);
				}
				this.$emit('change', value);
			}
		}
	}
});
