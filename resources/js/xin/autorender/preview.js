import * as xin from "../index";

//preview
$(function() {
	$(document).on('click', '[data-preview]', function(e) {
		const options = xin.getDataOptions(this, 'preview');

		if (!options.urls) {
			options.urls = [];
			const warpper = $(this);
			warpper.find('img').each(function(index, item) {
				options.urls.push(item.src);
				if (e.target === item) {
					options.current = index;
				}
			});
		}

		xin.preview(options);
		return false;
	});
});
