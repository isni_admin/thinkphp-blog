import * as xin from '../index';

// layer window
$(function() {
	$(document).on('click', '[data-open]', function() {
		const self = $(this);
		const url = self.attr('href') || self.attr('url') || self.attr('action') || self.data('url');
		const options = xin.getDataOptions(self, 'open');
		options.content = options.content || url;
		xin.open(options);
		return false;
	});
});
