// 渲染 layui 相关组件
$(function() {
	if (!window.layui) {
		return;
	}

	// switch
	layui.form.on('switch', function(data) {
		// const self = $(data.elem);
		// let value = self.attr('lay-value');
		// if (value) {
		// 	value = value.split('|', 2);
		// 	self.val(value[data.elem.checked ? 0 : 1]);
		// }
		$(data.elem).trigger('change');
	});

	// select
	layui.form.on('select', function(data) {
		$(data.elem).trigger('change');
		data.elem.oldValue = data.elem.value;
	});
	lay('.layui-form select').each(function(index, it) {
		it.oldValue = it.value;
	});

	layui.form.render();

	//轮播图
	$('[data-carousel]').each(function() {
		const options = $.extend({
			elem: this,
			width: '100%'//设置容器宽度
			// arrow: 'always' //始终显示箭头
			//,anim: 'updown' //切换动画方式
		}, xin.getDataOptions(this, 'carousel'));
		layui.carousel.render(options);
	});
});
