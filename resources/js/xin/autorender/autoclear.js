// handler
$(function() {
	const handler = function() {
		const self = $(this);
		if (self.val() === '') {
			self.next('i').hide();
		} else {
			self.next('i').show();
		}
	};

	$(document).on('click', 'input[data-autoclear]+i', function() {
		const self = $(this);
		self.prev('input').val('');
		self.hide();
	});

	$(document).on('keyup', 'input[data-autoclear]', handler);

	$('input[autoclear]').each(handler);
});
