import * as xin from "../index";

//img error auto
$(function() {
	$('img[data-error]').each(function() {
		const self = $(this);
		xin.imgError({
			el: this,
			url: self.data('error')
		});
	});
});
