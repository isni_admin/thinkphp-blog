import * as xin from "../index";

// Tooltips
$(function() {
	$(document).on('mouseenter', '[data-tip]', function() {
		const self = $(this);
		const options = $.extend({
			time: 1000,
			tips: 3
		}, xin.getDataOptions(self, 'tip'));

		options.content = self.data('tip');
		if (!options.content) return;

		options.el = this;
		xin.tooltip(options);
	});
});
