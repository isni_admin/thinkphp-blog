import * as xin from "../index";
//init date or time
$(function() {
	$('[data-datetime]').each(function() {
		//type:year,month,date,time,datetime
		//range:范围选择,true //或 range: '~' 来自定义分割字符
		const options = $.extend({
			elem: this,
			trigger: 'click',
			calendar: true
		}, xin.getDataOptions(this, 'datetime'));
		xin.datepicker(options);
	});
});
