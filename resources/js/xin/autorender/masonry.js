import * as xin from "../index";

//masonry
$(function() {
	$('[data-masonry]').each(function() {
		const self = $(this);

		const options = $.extend({
			gutter: 30,
			isAnimated: true
		}, xin.getDataOptions(self, 'masonry'));

		self.masonry(options);
	});
});
