<?php
/**
 * Talents come from diligence, and knowledge is gained by accumulation.
 *
 * @author: 晋<657306123@qq.com>
 *
 */

namespace app\admin\controller;

use app\admin\Controller;
use Xin\Thinkphp5\Controller\CURD;
use app\common\facade\View;
use app\common\model\User as UserModel;

/**
 * 用户管理
 *
 *
 */
class UserController extends Controller{

	use CURD {
		create as protected;
	}

	/**
	 * 获取广告位列表
	 *
	 * @throws \think\exception\DbException
	 */
	public function index(){
		$data = UserModel::order('id desc')->paginate($this->request->limit(), false, [
			'page'  => $this->request->page(),
			'query' => $this->request->get(),
		]);

		$this->assign('data', $data);

		return View::fetch();
	}

}
