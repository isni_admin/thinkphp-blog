<?php
namespace app\admin\controller;

use app\admin\Controller;
use app\common\facade\View;
use app\common\model\Post as PostModel;

class IndexController extends Controller{

	/**
	 * 首页
	 *
	 * @return mixed
	 */
	public function index(){
		$this->assignArticleStatistics();
		$this->assignUserStatistics();

		return View::fetch();
	}

	/**
	 * 文章统计
	 */
	private function assignArticleStatistics(){
		$articleCount = PostModel::count('id');
		$this->assign('article_count', $articleCount);

		$articleAuditedCount = PostModel::where('status', 0)->count();
		$this->assign('article_audited_count', $articleAuditedCount);
	}

	/**
	 * 用户统计
	 */
	private function assignUserStatistics(){
		// 本周微信访问量
		$this->assign('to_week_wechat_pv_count', 0);

		// 本周支付宝访问量
		$this->assign('to_week_alipay_pv_count', 0);

		// 本周微信活跃用户
		$this->assign('to_month_wechat_uv_count', 0);

		// 本周支付宝访问量
		$this->assign('to_month_alipay_uv_count', 0);

		// 本周活跃用户列表
		$this->assign('to_week_uv_list', 0);
	}



}
