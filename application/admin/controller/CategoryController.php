<?php
/**
 * Talents come from diligence, and knowledge is gained by accumulation.
 *
 * @author: 晋<657306123@qq.com>
 */

namespace app\admin\controller;

use app\admin\Controller;
use app\common\facade\View;
use app\common\model\Category as CategoryModel;
use Xin\Support\Arr;
use Xin\Thinkphp5\Controller\CURD;

/**
 * 分类管理
 */
class CategoryController extends Controller{

	use CURD {
		showCreateView as showCreateView2;
		showUpdateView as showUpdateView2;
	}

	/**
	 * 分类管理
	 *
	 * @return mixed
	 * @throws \think\exception\DbException
	 */
	public function index(){
		$categories = CategoryModel::order('sort', 'asc')->select();

		//		$data = Arr::tree($categories->toArray(), function($level, &$item){
		//			$item['level'] = $level;
		//		});
		//		$data = Arr::treeToList($data);
		$data = self::treeToList($categories->toArray());
		$this->assign("data", $data);

		$this->assignTreeCategory($categories->toArray());
		return View::fetch();
	}

	/**
	 * @inheritDoc
	 * @throws \think\db\exception\DataNotFoundException
	 * @throws \think\db\exception\ModelNotFoundException
	 * @throws \think\exception\DbException
	 */
	protected function showCreateView(){
		$this->assignTreeCategory();
		return $this->showCreateView2();
	}

	/**
	 * @inheritDoc
	 * @throws \think\db\exception\DataNotFoundException
	 * @throws \think\db\exception\ModelNotFoundException
	 * @throws \think\exception\DbException
	 */
	protected function showUpdateView($model){
		$this->assignTreeCategory();
		return $this->showUpdateView2($model);
	}

	/**
	 * 删除指定资源
	 */
	public function delete(){
		$ids = $this->request->ids();

		//检查是否有子分类 计算两个数组交集
		$pids = CategoryModel::where('pid', 'in', $ids)->column('pid');
		$pids = array_intersect($pids, $ids);

		if(!empty($pids)){
			$titles = implode("、", CategoryModel::select($pids)->column("title"));
			$this->error("请先删除【{$titles}】下的子分类！");
		}

		if(CategoryModel::destroy($ids) === false){
			$this->error('删除失败！');
		}

		$this->success('已删除！');
	}

	/**
	 * 向页面赋值分类列表
	 *
	 * @param array $data
	 * @throws \think\db\exception\DataNotFoundException
	 * @throws \think\db\exception\ModelNotFoundException
	 * @throws \think\exception\DbException
	 */
	private function assignTreeCategory(array $data = null){
		$data = self::treeToList($data);
		$data = array_merge([0 => ['id' => 0, 'title' => '顶级分类']], $data);

		$this->assign('categories', $data);
	}

	/**
	 * 解析树形列表
	 *
	 * @param array $data
	 * @return array
	 * @throws \think\db\exception\DataNotFoundException
	 * @throws \think\db\exception\ModelNotFoundException
	 * @throws \think\exception\DbException
	 */
	public static function treeToList(array $data = null){
		if(is_null($data)){
			$field = 'id,title,pid';
			$data = CategoryModel::field($field)->order('sort', 'asc')->select();
			$data = $data->toArray();
		}

		$data = Arr::tree($data, function($level, &$val){
			$tmp_str = '';
			for($i = 0; $i < $level - 1; $i++) $tmp_str = $tmp_str.str_repeat("&nbsp;", 5)."│";
			$tmp_str .= str_repeat("&nbsp;", 4)."┝";
			$val['level'] = $level;
			$val['title'] = $level == 0 ? $val['title']."&nbsp;" : $tmp_str.$val['title']."&nbsp;";
		});

		return Arr::treeToList($data);
	}
}
