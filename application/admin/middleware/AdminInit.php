<?php
/**
 * Talents come from diligence, and knowledge is gained by accumulation.
 *
 * @author: 晋<657306123@qq.com>
 */

namespace app\admin\middleware;

use app\admin\service\MenuService;
use app\common\facade\View;
use app\Request;
use think\App;
use think\Config;
use Xin\Auth\UserInterface;
use Xin\Thinkphp5\Auth\Facade\Auth;

/**
 * Class AdminInit
 */
class AdminInit{

	/**
	 * @var \think\App
	 */
	private $app;

	/**
	 * @var \app\Request
	 */
	private $request;

	/**
	 * @var \think\Config
	 */
	private $config;

	/**
	 * AdminInit constructor.
	 *
	 * @param \think\App    $app
	 * @param \app\Request  $request
	 * @param \think\Config $config
	 */
	public function __construct(App $app, Request $request, Config $config){
		$this->app = $app;
		$this->request = $request;
		$this->config = $config;

		$this->registerUserService();
	}

	/**
	 * @param \app\Request $request
	 * @param \Closure     $next
	 * @return mixed
	 */
	public function handle($request, \Closure $next){
		$this->menuHandle();
		return $next($request);
	}

	/**
	 * 注册用户服务
	 */
	private function registerUserService(){
		$user = Auth::guard('admin');
		$this->app->bindTo('user', $user);
		$this->app->bindTo(UserInterface::class, $user);
	}

	/**
	 * 菜单处理
	 */
	private function menuHandle(){
		$menu = new MenuService(
			$this->config->pull('menus')
		);
		$menu->generate();

		View::share('__ASIDE_MENUS__', $menu->getMenus());
		View::share('__BREADCRUMB__', $menu->getBreadcrumb());
	}
}
