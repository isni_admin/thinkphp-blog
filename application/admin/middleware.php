<?php
/**
 * Talents come from diligence, and knowledge is gained by accumulation.
 *
 * @author: 晋<657306123@qq.com>
 * @date: 2019/8/16 22:12
 */
use app\admin\middleware\AdminInit;
use app\admin\middleware\CheckAuth;
use app\common\middleware\ViewInit;

return [
	ViewInit::class,
	AdminInit::class,
	CheckAuth::class,
];
