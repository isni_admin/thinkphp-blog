<?php
/**
 * Talents come from diligence, and knowledge is gained by accumulation.
 *
 * @author: 晋<657306123@qq.com>
 * @date: 2019/7/31 11:06
 */
return [
	[
		'title' => '首页',
		'name'  => 'index/index',
		'icon'  => 'layui-icon-home',
	],
	//	[
	//		'title' => '用户',
	//		'name'  => '',
	//		'icon'  => 'layui-icon-user',
	//		'child' => [
	//			[
	//				'title' => '用户管理',
	//				'name'  => 'user/index',
	//				'icon'  => '',
	//			],
	//		],
	//	],
	[
		'title' => '文档',
		'name'  => '',
		'icon'  => 'layui-icon-read',
		'child' => [
			[
				'title' => '文章管理',
				'name'  => 'article/index',
				'icon'  => '',
				'child' => [
					[
						'title' => '新增文章',
						'name'  => 'article/create',
					],
					[
						'title' => '更新文章',
						'name'  => 'article/update',
					],
				],
			],
			[
				'title' => '分类管理',
				'name'  => 'category/index',
				'icon'  => '',
				'child' => [
					[
						'title' => '新增分类',
						'name'  => 'category/create',
					],
					[
						'title' => '更新分类',
						'name'  => 'category/update',
					],
				],
			],
			//			[
			//				'title' => '标签管理',
			//				'name'  => 'badge/index',
			//				'icon'  => '',
			//			],
		],
	],
	[
		'title' => '前台',
		'name'  => '',
		'icon'  => 'layui-icon-website',
		'child' => [
			[
				'title' => '广告位管理',
				'name'  => 'advertising/index',
				'icon'  => '',
				'child' => [
					[
						'title' => '新增广告',
						'name'  => 'advertising/create',
					],
					[
						'title' => '更新广告',
						'name'  => 'advertising/update',
					],
				],
			],
		],
	],
	[
		'title' => '系统',
		'name'  => '',
		'icon'  => 'layui-icon-website',
		'child' => [
			[
				'title' => '网站配置',
				'name'  => 'setting/group',
				'icon'  => 'layui-icon-set',
			],
			[
				'title' => '配置管理',
				'name'  => 'setting/index',
				'icon'  => '',
				'child' => [
					[
						'title' => '新增配置',
						'name'  => 'setting/create',
					],
					[
						'title' => '更新配置',
						'name'  => 'setting/update',
					],
					[
						'title' => '网站配置排序',
						'name'  => 'setting/sort',
					],
				],
			],
			[
				'title' => '修改密码',
				'name'  => 'index/modifyPwd',
				'icon'  => '',
			],
		],
	],
	//	[
	//		'title'    => '',
	//		'name'      => '',
	//		'icon'     => '',
	//		'child' => [
	//
	//		],
	//	],
];
