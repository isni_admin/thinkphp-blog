<?php
/**
 * I know no such things as genius,it is nothing but labor and diligence.
 *
 * @copyright (c) 2015~2019 BD All rights reserved.
 * @license       http://www.apache.org/licenses/LICENSE-2.0
 * @author        <657306123@qq.com> LXSEA
 */
namespace app\admin;

use app\admin\middleware\CheckAuth;
use app\BaseController;
use Xin\Thinkphp5\Auth\Facade\Auth;

/**
 * 后台基础控制器
 *
 * @property-read \think\App              $app
 * @property-read \Xin\Auth\UserInterface $user
 * @package app\common\controller
 */
class Controller extends BaseController{

	/**
	 * 验证失败直接抛出异常
	 *
	 * @var bool
	 */
	protected $failException = true;

	/**
	 * 中间件
	 *
	 * @var array
	 */
	protected $middleware = [
		CheckAuth::class,
	];

	/**
	 * 用户接口
	 *
	 * @var \Xin\Auth\UserInterface
	 */
	protected $user;

	/**
	 * 初始化
	 */
	protected function initialize(){
		$this->user = Auth::guard();
	}

}
