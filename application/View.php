<?php
/**
 * Talents come from diligence, and knowledge is gained by accumulation.
 *
 * @author: 晋<657306123@qq.com>
 */

namespace app;

use think\App;
use think\Container;
use think\facade\Request;
use think\Paginator;
use think\View as BaseView;
use Xin\Thinkphp5\Hint\Facade\Hint;

class View extends BaseView{

	/**
	 * @var \think\Env
	 */
	private $env;

	/**
	 * @var \think\Config
	 */
	private $config;

	/**
	 * @var \think\Request
	 */
	private $request;

	/**
	 * View constructor.
	 *
	 * @param \think\App $app
	 */
	public function __construct(App $app = null){
		$app = is_null($app) ? Container::get('app') : $app;
		$this->env = $app['env'];
		$this->config = $app['config'];
		$this->request = $app['request'];
	}

	//	/**
	//	 * 配置数据
	//	 *
	//	 * @param array $config
	//	 * @return static
	//	 */
	//	public function config(array $config){
	//		return $this->engine()->config($config);
	//	}

	/**
	 * 设置主题
	 *
	 * @param string $theme
	 * @return static
	 */
	public function setTheme(string $theme){
		$theme = $this->env->get('module_path')."view/{$theme}/";
		return self::config([
			'view_path' => $theme,
		]);
	}

	/**
	 * 设置信息头
	 *
	 * @param string $title
	 * @param string $keywords
	 * @param string $description
	 * @return static
	 */
	public function setMeta($title = null, $keywords = null, $description = null){
		$assign = [];

		if(!empty($title)){
			$assign['meta_title'] = $title." - ".$this->config->get('web.site_title');
			$assign['page_title'] = $title;
		}

		$description = $description ? $description : $this->config->get('web.site_description');
		$assign['meta_description'] = $description;

		if($keywords){
			$keywords .= ','.$this->config->get('web.site_keywords');
		}else{
			$keywords = $this->config->get('web.site_keywords');
		}
		$assign['meta_keywords'] = $keywords;

		return $this->assign($assign);
	}

	/** @noinspection PhpDocMissingThrowsInspection */
	/** @noinspection PhpSignatureMismatchDuringInheritanceInspection */
	/**
	 * 加载模板输出
	 *
	 * @access protected
	 * @param string $template 模板文件名
	 * @param array  $vars 模板输出变量
	 * @param array  $config
	 * @param bool   $renderContent
	 * @return string
	 */
	public function fetch($template = '', $vars = [], $config = [], $renderContent = false){
		if((Request::isAjax() || Request::isCli())){
			try{
				$reflection = new \ReflectionClass($this);
				$dataProperty = $reflection->getProperty('data');
				$dataProperty->setAccessible(true);
				$data = $dataProperty->getValue($this);
				$data = array_merge($data, $vars);

				if(isset($data['data']) && $data['data'] instanceof \think\Paginator){
					/**@var $paginator Paginator */
					$paginator = $data['data'];
					$data = array_merge($data, [
						'data'        => $paginator->items(),
						'total'       => $paginator->total(),
						'listRows'    => $paginator->listRows(),
						'currentPage' => $paginator->currentPage(),
						'lastPage'    => $paginator->lastPage(),
					]);
				}

				return Hint::result([], $data);
			}catch(\Exception $e){
				throw new \RuntimeException("错误的数据", 0, $e);
			}
		}

		/** @noinspection PhpUnhandledExceptionInspection */
		return parent::fetch($template, $vars, $config, $renderContent);
	}
}
