<?php
/**
 * Talents come from diligence, and knowledge is gained by accumulation.
 *
 * @author: 晋<657306123@qq.com>
 * @date: 2019/7/30 16:36
 */

namespace app\common\model;

use think\facade\Config;
use think\Model;
use Xin\Auth\UserProviderInterface;
use Xin\Thinkphp5\Auth\UserProvider;

/**
 * 管理用户模型
 *
 * @property-read integer $id
 * @property-read string  $username
 * @property-read string  password
 */
class Admin extends Model implements UserProviderInterface{

	use  UserProvider;

	/**
	 * 密码是否输入正确
	 *
	 * @param string $pwd
	 * @return string
	 */
	public function isPasswordOk(string $pwd){
		return $this->password == self::encrypt($pwd);
	}

	/**
	 * 加密密码
	 *
	 * @param string $pwd
	 * @return string
	 */
	public static function encrypt($pwd){
		$key = Config::get('app.key');
		return md5(sha1($pwd).$key);
	}

	public function validatePassword($user, $password){
		return $user->isPasswordOk($password);
	}
}
