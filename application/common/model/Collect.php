<?php
/**
 * Talents come from diligence, and knowledge is gained by accumulation.
 *
 * @author: 晋<657306123@qq.com>
 */

namespace app\common\model;

use app\common\model\traits\AppScope;
use think\model\Pivot;

/**
 * Class Collect
 *
 * @property int topic_id
 */
class Collect extends Pivot{

	use AppScope;

	/**
	 * 专题收藏
	 */
	const CATEGORY_TYPE = 0;

	/**
	 * 图集收藏
	 */
	const GALLERY_TYPE = 1;

	/**
	 * 文章收藏
	 */
	const POST_TYPE = 2;

	/**
	 * 图片收藏
	 */
	const IMG_TYPE = 3;

	/**
	 * 浏览收藏分类
	 */
	const VISIT_CATEGORY_TYPE = 4;

	/**
	 * 浏览收藏图集
	 */
	const VISIT_GALLERY_TYPE = 5;

	/**
	 * 浏览收藏文章
	 */
	const VISIT_POST_TYPE = 6;

	/**
	 * 浏览收藏图片
	 */
	const VISIT_IMG_TYPE = 7;

	/**
	 * @var int
	 */
	protected $defaultSoftDelete = 0;

	/**
	 * 管理文章模型
	 *
	 * @return \think\model\relation\HasOne
	 */
	protected function hasOnePost(){
		return $this->hasOne('Post', 'id', 'topic_id')
			->field('id,title,cover,is_original')
			->bind('title,cover,is_original');
	}

	/**
	 * 关联收藏的文章模型
	 *
	 * @return \think\model\relation\HasOne
	 */
	public function post(){
		return $this->hasOnePost();
	}

	/**
	 * 关联浏览过的文章模型
	 *
	 * @return \think\model\relation\HasOne
	 */
	public function visitPost(){
		return $this->hasOnePost();
	}

	/**
	 * 管理分类模型
	 *
	 * @return \think\model\relation\HasOne
	 */
	protected function hasOneCategory(){
		return $this->hasOne('Category', 'id', 'topic_id')
			->field('id,title,cover,description')
			->bind('title,cover,description');
	}

	/**
	 * 关联收藏的分类模型
	 *
	 * @return \think\model\relation\HasOne
	 */
	public function category(){
		return $this->hasOneCategory();
	}

	/**
	 * 关联浏览过的分类模型
	 *
	 * @return \think\model\relation\HasOne
	 */
	public function visitCategory(){
		return $this->hasOneCategory();
	}

	/**
	 * 关联图片模型
	 *
	 * @return \think\model\relation\HasOne
	 */
	public function hasOneImage(){
		return $this->hasOne('GalleryItem', 'id', 'topic_id')
			->field('id,title,is_original');
	}

	/**
	 * 关联收藏的图片模型
	 *
	 * @return \think\model\relation\HasOne
	 */
	public function image(){
		return $this->hasOneImage();
	}

	/**
	 * 关联浏览过的图片模型
	 *
	 * @return \think\model\relation\HasOne
	 */
	public function visitImage(){
		return $this->hasOneImage();
	}

	/**
	 * 关联图集模型
	 *
	 * @return \think\model\relation\HasOne
	 */
	public function hasOneGallery(){
		return $this->hasOne('Gallery', 'id', 'topic_id')
			->field('id,title,cover,description')
			->bind('title,cover,description');
	}

	/**
	 * 关联收藏的图集模型
	 *
	 * @return \think\model\relation\HasOne
	 */
	public function gallery(){
		return $this->hasOneGallery();
	}

	/**
	 * 关联浏览过的图集模型
	 *
	 * @return \think\model\relation\HasOne
	 */
	public function visitGallery(){
		return $this->hasOneGallery();
	}

	/**
	 * 关联用户模型
	 *
	 * @return \think\model\relation\HasMany
	 */
	public function user(){
		return $this->hasMany('User', 'id', 'uid')
			->field('id,nickname,avatar');
	}

	/**
	 * 转化类型为关联键名
	 *
	 * @param int $type
	 * @return string|null
	 */
	public static function resolveTypeToWith($type){
		if(self::CATEGORY_TYPE == $type){
			return "category";
		}elseif(self::POST_TYPE == $type){
			return "post";
		}elseif(self::IMG_TYPE == $type){
			return "image";
		}elseif(self::GALLERY_TYPE == $type){
			return "gallery";
		}elseif(self::VISIT_CATEGORY_TYPE == $type){
			return "visitCategory";
		}elseif(self::VISIT_POST_TYPE == $type){
			return "visitPost";
		}elseif(self::VISIT_IMG_TYPE == $type){
			return "visitImage";
		}elseif(self::VISIT_GALLERY_TYPE == $type){
			return "visitGallery";
		}
		return null;
	}
}
