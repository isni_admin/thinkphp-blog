<?php
/**
 * I know no such things as genius,it is nothing but labor and diligence.
 *
 * @copyright (c) 2015~2019 BD All rights reserved.
 * @license       http://www.apache.org/licenses/LICENSE-2.0
 * @author        <657306123@qq.com> LXSEA
 */

namespace app\common\validate;

use think\Validate;

/**
 * 分类验证器
 */
class AdvertisingValidate extends Validate{

	/**
	 * 验证规则
	 *
	 * @var array
	 */
	protected $rule = [
		'title' => 'require|length:2,48',
		'name'  => 'require|alphaDash|length:3,48|unique:advertising',
	];

	/**
	 * 字段信息
	 *
	 * @var array
	 */
	protected $field = [
		'title' => '分类标题',
		'name'  => '唯一标识',
	];

	/**
	 * 验证消息
	 *
	 * @var array
	 */
	protected $message = [
	];

	/**
	 * 情景模式
	 *
	 * @var array
	 */
	protected $scene = [];
}
