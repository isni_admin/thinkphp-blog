<?php
/**
 * Talents come from diligence, and knowledge is gained by accumulation.
 *
 * @author: 晋<657306123@qq.com>
 */

namespace app\common\validate;

use think\Validate;

/**
 * 广告项
 */
class AdvertisingItemValidate extends Validate{

	/**
	 * 验证规则
	 *
	 * @var array
	 */
	protected $rule = [
		'title'        => 'length:2,48',
		'cover'        => 'require',
		'origin_cover' => 'requireIf:click_type,0',
		'url'          => 'requireIf:click_type,1|url',
		'end_time'     => 'after:now',
	];

	/**
	 * 字段信息
	 *
	 * @var array
	 */
	protected $field = [
		'title'        => '标题',
		'cover'        => '封面图',
		'origin_cover' => '原图',
		'url'          => '链接',
		'end_time'     => '结束时间',
	];

	/**
	 * 验证消息
	 *
	 * @var array
	 */
	protected $message = [
	];

	/**
	 * 情景模式
	 *
	 * @var array
	 */
	protected $scene = [];
}
