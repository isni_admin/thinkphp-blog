<?php
/**
 * I know no such things as genius,it is nothing but labor and diligence.
 *
 * @copyright (c) 2015~2019 BD All rights reserved.
 * @license       http://www.apache.org/licenses/LICENSE-2.0
 * @author        <657306123@qq.com> LXSEA
 */

namespace app\common\validate;

use think\Validate;

/**
 * 文章验证器
 */
class PostValidate extends Validate{

	/**
	 * 验证规则
	 *
	 * @var array
	 */
	protected $rule = [
		'title'       => 'require|length:2,48',
		'content'     => 'require',
		'category_id' => 'require',
	];

	/**
	 * 字段信息
	 *
	 * @var array
	 */
	protected $field = [
		'title'       => '文章标题',
		'content'     => '文章正文',
		'category_id' => '所属分类',
	];

	/**
	 * 情景模式
	 *
	 * @var array
	 */
	protected $scene = [];
}
