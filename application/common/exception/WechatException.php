<?php
/**
 * Talents come from diligence, and knowledge is gained by accumulation.
 *
 * @author: 晋<657306123@qq.com>
 */

namespace app\common\exception;

class WechatException extends \Exception{

	/**
	 * @var array
	 */
	protected $response;

	/**
	 * @return array
	 */
	public function getResponse(){
		return $this->response;
	}

	/**
	 * @param array $response
	 */
	public function setResponse($response){
		$this->response = $response;
	}

}
