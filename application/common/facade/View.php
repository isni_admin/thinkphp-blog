<?php
/**
 * Talents come from diligence, and knowledge is gained by accumulation.
 *
 * @author: 晋<657306123@qq.com>
 */
namespace app\common\facade;

use think\Facade;

/**
 * @see \app\View
 * @mixin \app\View
 * @method \app\View init(mixed $engine = [], array $replace = []) static 初始化
 * @method \app\View share(mixed $name, mixed $value = '') static 模板变量静态赋值
 * @method \app\View assign(mixed $name, mixed $value = '') static 模板变量赋值
 * @method \app\View config(mixed $name, mixed $value = '') static 配置模板引擎
 * @method \app\View exists(mixed $name) static 检查模板是否存在
 * @method \app\View filter(Callable $filter) static 视图内容过滤
 * @method \app\View engine(mixed $engine = []) static 设置当前模板解析的引擎
 * @method string fetch(string $template = '', array $vars = [], array $config = [], bool $renderContent = false) static 解析和获取模板内容
 * @method string display(string $content = '', array $vars = [], array $config = []) static 渲染内容输出
 */
class View extends Facade{

	/**
	 * 获取当前Facade对应类名（或者已经绑定的容器对象标识）
	 *
	 * @access protected
	 * @return string
	 */
	protected static function getFacadeClass(){
		return 'view';
	}
}
