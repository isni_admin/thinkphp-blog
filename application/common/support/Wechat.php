<?php
/**
 * Talents come from diligence, and knowledge is gained by accumulation.
 *
 * @author: 晋<657306123@qq.com>
 */

namespace app\common\support;

use EasyWeChat\Factory;
use think\facade\Config;

final class Wechat{

	/**
	 * @var \EasyWeChat\OfficialAccount\Application
	 */
	private static $OFFICIAL = null;

	/**
	 * 获取开发平台账号
	 *
	 * @return \EasyWeChat\OfficialAccount\Application
	 */
	public static function getOfficial(){
		if(is_null(self::$OFFICIAL)){
			$config = Config::get('wechat.official');
			self::$OFFICIAL = Factory::officialAccount($config);
		}
		
		return self::$OFFICIAL;
	}

}
