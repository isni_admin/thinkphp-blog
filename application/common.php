<?php

if(!function_exists('listen')){
	/**
	 * 监听行为
	 *
	 * @param string $tag
	 */
	function listen($tag){
		app('hook')->listen($tag);
	}
}

if(!function_exists('weight')){
	/**
	 * 执行小挂件
	 *
	 * @param string $url
	 * @param array  $vars
	 * @param bool   $appendSuffix
	 * @return mixed
	 */
	function weight($url, $vars = [], $appendSuffix = true){
		return action($url, $vars, 'weight', $appendSuffix);
	}
}

if(!function_exists('logic')){
	/**
	 * 获取业务控制器
	 *
	 * @param string $name
	 * @param bool   $appendSuffix
	 * @return mixed
	 */
	function logic($name, $appendSuffix = true){
		return controller($name, 'logic', $appendSuffix);
	}
}

if(!function_exists('logic_action')){
	/**
	 * 执行业务控制器的方法
	 *
	 * @param string $name
	 * @param array  $vars
	 * @param bool   $appendSuffix
	 * @return mixed
	 */
	function logic_action($name, $vars = [], $appendSuffix = true){
		return action($name, $vars, 'logic', $appendSuffix);
	}
}
/**
 * Ip地址转地理位置
 *
 * @param string $ip
 * @return array|null
 */
function ip2location($ip = ''){
	$ipLocation = new \xin\iplocation\IpLocation();
	return $ipLocation->getLocation($ip);
}

/**
 * 获取属性类型
 *
 * @param string $type
 * @return array|mixed|null
 */
function get_attribute_type($type = null){
	static $_type = [
		'number'   => ['数字', 'int(11) UNSIGNED NOT NULL'],
		'string'   => ['字符串', 'varchar(255) NOT NULL'],
		'textarea' => ['文本框', 'text NOT NULL'],
		'date'     => ['日期', 'int(10) UNSIGNED NOT NULL'],
		//		'time'     => ['时间', 'int(10) UNSIGNED NOT NULL'],
		'datetime' => ['日期时间', 'int(10) UNSIGNED NOT NULL'],
		'switch'   => ['布尔', 'tinyint(2) UNSIGNED NOT NULL'],
		'select'   => ['枚举', 'varchar(50) NOT NULL'],
		'radio'    => ['单选', 'varchar(12) NOT NULL'],
		'checkbox' => ['多选', 'varchar(128) NOT NULL'],
		'editor'   => ['编辑器', 'text NOT NULL'],
		'picture'  => ['上传图片', 'varchar(128) NOT NULL'],
		'file'     => ['上传附件', 'varchar(128) NOT NULL'],
	];
	if(is_null($type)) return $_type;

	return $type ? isset($_type[$type]) ? $_type[$type] : null : null;
}

/**
 * 生成计算位置字段
 *
 * @param float  $longitude
 * @param float  $latitude
 * @param string $lng_name
 * @param string $lat_name
 * @param string $as_name
 * @return string
 */
function build_mysql_distance_field($longitude, $latitude, $lng_name = 'longitude', $lat_name = 'latitude', $as_name = 'distance'){
	return "ROUND(6378.138*2*ASIN(SQRT(POW(SIN(({$latitude}*PI()/180-{$lat_name}*PI()/180)/2),2)+COS({$latitude}*PI()/180)*COS({$lat_name}*PI()/180)*POW(SIN(({$longitude}*PI()/180-{$lng_name}*PI()/180)/2),2)))*1000) AS {$as_name}";
}

/**
 * 获取数据库数据
 *
 * @param string $table
 * @param array  $where
 * @param string $field
 * @param string $order
 * @param string $page
 * @return array|string|\think\Collection
 * @throws \think\db\exception\DataNotFoundException
 * @throws \think\db\exception\ModelNotFoundException
 * @throws \think\exception\DbException
 */
function table_rows($table, $where = [], $field = '*', $order = '', $page = ''){
	return \think\Db::name($table)->field($field)->where($where)->order($order)->page($page)->select();
}

/**
 * 获取数据库数据
 *
 * @param string $table
 * @param string $field
 * @param array  $where
 * @param string $key
 * @param string $order
 * @param string $page
 * @return array
 */
function table_column($table, $field, $where = [], $key = '', $order = '', $page = ''){
	return \think\Db::name($table)->where($where)->order($order)->page($page)->column($field, $key);
}

/**
 * 获取数据库值
 *
 * @param string       $table
 * @param string       $field 字段名
 * @param array|string $where
 * @param mixed        $default 默认值
 * @return mixed
 */
function table_value($table, $field, $where, $default = null){
	return \think\Db::name($table)->where($where)->value($field, $default);
}

/**
 * 获取图片地址
 *
 * @param string $path
 * @return mixed
 */
function get_cover_path($path){
	if(strpos($path, '/') === 0){
		return request()->domain().$path;
	}
	return $path;
}

/**
 * 获取封面信息
 *
 * @param int    $pictureId
 * @param string $field
 * @return mixed
 * @throws \think\db\exception\DataNotFoundException
 * @throws \think\db\exception\ModelNotFoundException
 * @throws \think\exception\DbException
 */
function get_cover($pictureId, $field = null){
	static $data = [];
	if(!isset($data[$pictureId])){
		$data[$pictureId] = \think\Db::name('picture')->where('id', $pictureId)->find();
	}
	$info =& $data[$pictureId];
	return ($field && $info) ? $info[$field] : $info;
}

/**
 * 获取分类列表
 *
 * @throws \think\db\exception\DataNotFoundException
 * @throws \think\db\exception\ModelNotFoundException
 * @throws \think\exception\DbException
 */
function get_category_tree(){
	$data = table_rows('category', [], '*', 'sort asc');
	$data = \Xin\Support\Arr::tree($data);
	return $data;
}

/**
 * 关键字分词
 *
 * @param string $keyword
 * @param int    $num 最大返回条数
 * @param int    $holdLength 保留字数
 * @return array
 */
function analysis_words($keyword, $num = 5, $holdLength = 48){
	if($keyword === null || $keyword === "") return [];
	if(mb_strlen($keyword) > $holdLength) $keyword = mb_substr($keyword, 0, 48);

	//执行分词
	$pa = new \xin\analysis\Analysis('utf-8', 'utf-8');
	$pa->setSource($keyword);
	$pa->startAnalysis();
	$result = $pa->getFinallyResult($num);
	if(empty($result)) return [$keyword];

	return array_unique($result);
}

/**
 * 编译查询关键字SQL
 *
 * @param string $keywords
 * @return array
 */
function build_keyword_sql($keywords){
	$keywords = analysis_words($keywords);
	return array_map(function($item){
		return "%{$item}%";
	}, $keywords);
}

/**
 * 优化资源路径
 *
 * @param string $uri
 * @param bool   $prefix
 * @return string
 */
function optimize_asset($uri, $prefix = false){
	$index = strpos($uri, '://');
	if($index === false){
		$uri = "//".request()->host().$uri;
	}elseif(0 !== $index){
		$uri = substr($uri, $index + 1);
	}

	if($prefix){
		if(is_bool($prefix)){
			$uri = 'http:'.$uri;
		}else{
			$uri = $prefix.':'.$uri;
		}
	}

	return $uri;
}
