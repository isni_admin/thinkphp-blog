<?php
/**
 * Talents come from diligence, and knowledge is gained by accumulation.
 *
 * @author: 晋<657306123@qq.com>
 * @date: 2019/8/16 22:16
 */
namespace app\api\service;

use app\common\contract\UserInterface;
use app\common\contract\UserTokenInterface;
use app\common\sys\Ajax;
use app\Request;
use think\Config;
use think\Session;

/**
 * 会员服务
 *
 * @package app\api\service
 */
class Member implements UserInterface, UserTokenInterface{

	/**
	 * @var \app\Request
	 */
	private $request;

	/**
	 * @var \think\Config
	 */
	private $config;

	/**
	 * @var \think\Session
	 */
	private $session;

	/**
	 * @var \app\common\model\User
	 */
	private $member = null;

	/**
	 * @var string
	 */
	private $token;

	/**
	 * Member constructor.
	 *
	 * @param \app\Request   $request
	 * @param \think\Config  $config
	 * @param \think\Session $session
	 */
	public function __construct(Request $request, Config $config, Session $session){
		$this->request = $request;
		$this->config = $config;
		$this->session = $session;
	}

	/**
	 * 获取用户令牌
	 *
	 * @return string
	 */
	public function getUserToken(){
		if(is_null($this->token)){
			if($this->request->has('session_id', 'get')){
				$this->token = $this->request->get('session_id/s', '', 'trim');
			}elseif($this->request->has('session_id', 'post')){
				$this->token = $this->request->post('session_id/s', '', 'trim');
			}else{
				$this->token = $this->request->header('session_id/s', '');
			}
		}

		return $this->token;
	}

	/**
	 * 设置用户令牌
	 *
	 * @param string $token
	 */
	public function setUserToken($token){
		$this->token = $token;
	}

	/**
	 * 获取用户信息
	 *
	 * @param string $field
	 * @param mixed  $default
	 * @param bool   $abort
	 * @return mixed
	 */
	public function get($field = null, $default = null, $abort = true){
		if(is_null($this->member)){
			$sessionID = $this->getUserToken();

			if($abort && empty($this->token)){
				Ajax::outputError('请登录...', -1);
			}

			if(empty($sessionID)){
				$this->member = false;
				return $default;
			}

			session_id($sessionID);
			$this->member = $this->session->get('user_info');
		}

		if($abort && empty($this->member)){
			Ajax::outputError('登录失效~', -1);
		}

		$userInfo =& $this->member;
		return $userInfo ? (empty($field) ? $userInfo : (isset($userInfo[$field]) ? $userInfo[$field] : $default)) : $default;
	}

	/**
	 * 获取用户id
	 *
	 * @param bool $abort
	 * @return int
	 */
	public function getId($abort = true){
		return $this->get('id', 0, $abort);
	}

	/**
	 * 设置用户信息
	 *
	 * @param array $info
	 * @return \app\common\model\User
	 */
	public function set($info){
		$this->get();

		if(is_null($this->member)) return null;

		$this->member->data($info);
		$this->session->set('user_info', $this->member);

		return $this->member;
	}

	/**
	 * 更新用户信息-保存到数据库
	 * @param array $data
	 * @return \app\common\model\User
	 */
	public function update($data){
		$this->get();

		if(is_null($this->member)) return null;

		$this->member->save($data);
		$this->session->set('user_info', $this->member);

		return $this->member;
	}

	/**
	 * 销毁
	 */
	public function destroy(){
		$this->session->destroy();
		$this->session->flush();
	}

}
