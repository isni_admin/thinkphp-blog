<?php
/**
 * Talents come from diligence, and knowledge is gained by accumulation.
 *
 * @author: 晋<657306123@qq.com>
 * @date: 2019/8/3 18:11
 */

namespace app\api\controller;

use app\api\ApiController;
use think\Db;
use Xin\Thinkphp5\Hint\Facade\Hint;

class IndexController extends ApiController{

	/**
	 * 基本配置
	 *
	 * @return \think\Response
	 */
	public function config1(){
		return Hint::result();
	}

	/**
	 * 获取广告位
	 *
	 * @return \think\response
	 * @throws \think\db\exception\DataNotFoundException
	 * @throws \think\db\exception\ModelNotFoundException
	 * @throws \think\exception\DbException
	 */
	public function banners(){
		$uid = $this->user->getUserId();
		$name = $this->request->param('name', '', 'trim');
		$data = Db::name('advertising')
			->field('id,name')
			->where('type', 1)
			->where('name', $name)->find();
		if(empty($data)){
			return Hint::result();
		}

		$field = 'id,title,cover,origin_cover,click_type,url';
		$data['imgs'] = Db::name('advertising_item')
			->field($field)
			->where('status', 1)
			->where('ad_id', $data['id'])
			->where('begin_time', '<', $this->request->time())
			->where('end_time', '>', $this->request->time())
			->order('sort asc')
			->select();

		return Hint::success($data);
	}

}
