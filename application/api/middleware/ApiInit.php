<?php
/**
 * Talents come from diligence, and knowledge is gained by accumulation.
 *
 * @author: 晋<657306123@qq.com>
 */

namespace app\api\middleware;

use app\Request;
use think\App;
use think\Config;
use Xin\Auth\UserInterface;
use Xin\Thinkphp5\Auth\Facade\Auth;
use Xin\Thinkphp5\Hint\ApiHint;

class ApiInit{

	/**
	 * @var \think\App
	 */
	private $app;

	/**
	 * @var \app\Request
	 */
	private $request;

	/**
	 * @var \think\Config
	 */
	private $config;

	/**
	 * ApiInit constructor.
	 *
	 * @param \think\App    $app
	 * @param \app\Request  $request
	 * @param \think\Config $config
	 */
	public function __construct(App $app, Request $request, Config $config){
		$this->app = $app;
		$this->request = $request;
		$this->config = $config;

		$this->app->bindTo('hint', ApiHint::class);

		$this->registerUserService();
	}

	/**
	 * @param \app\Request $request
	 * @param \Closure     $next
	 * @return mixed
	 */
	public function handle($request, \Closure $next){
		return $next($request);
	}

	/**
	 * 注册用户服务
	 */
	private function registerUserService(){
		$user = Auth::guard('api');
		$this->app->bindTo('user', $user);
		$this->app->bindTo(UserInterface::class, $user);
	}

}
