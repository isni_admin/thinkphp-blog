<?php
namespace app\index\controller;

use app\index\Controller;

class IndexController extends Controller{

	/**
	 * 首页
	 *
	 * @return mixed
	 */
	public function index(){
		//		$this->redirect('admin/login/login');
		return $this->fetch();
	}

}
