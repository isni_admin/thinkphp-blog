<?php
/**
 * Talents come from diligence, and knowledge is gained by accumulation.
 *
 * @author: 晋<657306123@qq.com>
 */

namespace app\index\controller;

use app\common\model\Post as PostModel;
use app\index\Controller;

class PostController extends Controller{

	/**
	 * 首页
	 *
	 * @return mixed
	 * @throws \think\exception\DbException
	 */
	public function index(){
		$keywords = $this->request->keywordsSql();

		$data = PostModel::when(!empty($keywords), [
			['title', 'like', $keywords,],
		])
			->where('status', 1)
			->paginate($this->request->limit(), false, [
				'page'  => $this->request->page(),
				'query' => $this->request->get(),
			]);

		$this->assign('data', $data);
		return $this->fetch();
	}

	/**
	 * 文章详情
	 *
	 * @param int $id
	 * @return mixed
	 * @throws \think\Exception
	 * @throws \think\db\exception\DataNotFoundException
	 * @throws \think\db\exception\ModelNotFoundException
	 * @throws \think\exception\DbException
	 */
	public function detail($id){
		/** @var PostModel $info */
		$info = PostModel::where('id', $id)->findOrFail();

		$info->setInc('view_count');
		$info->view_count++;

		$this->assign('info', $info);
		return $this->fetch();
	}
}
