<?php
/**
 * The following code, none of which has BUG.
 *
 * @author: BD<657306123@qq.com>
 * @date: 2020/2/24 0:33
 */
return [
	//超级管理员ID
	'admin_administrator' => 1,

	// 总站点访问签名
	'admin_visit_sign'    => env('APP_VISIT_SIGN'),

	// 默认选项
	'defaults'            => [
		'guard' => 'user',
	],

	// 守卫者
	'guards'              => [
		'admin' => [
			'driver'    => 'session',
			'provider'  => 'admin',
			'guest_url' => 'login/login',
		],
		'user' => [
			'driver'    => 'session',
			'provider'  => 'user',
			'guest_url' => 'login/login',
		],
		'api'   => [
			'driver'   => 'token_session',
			'provider' => 'user',
		],
	],

	// 提供者
	'providers'           => [
		'admin' => \app\common\model\Admin::class,
		'user'  => \app\common\model\User::class,
	],
];
