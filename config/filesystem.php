<?php
/**
 * The following code, none of which has BUG.
 *
 * @author: BD<liuxingwu@duoguan.com>
 * @date: 2019/8/8 16:10
 */
use think\facade\Env;

return [
	'driver'  => Env::get('upload_driver'),

	// 驱动列表
	'drivers' => [
		// 七牛云
		'qiniu'  => [
			'ak'     => Env::get('upload_qiniu_ak'),
			'sk'     => Env::get('upload_qiniu_sk'),
			'bucket' => Env::get('upload_qiniu_bucket'),
			'domain' => '',
		],

		// 阿里云
		'aliyun' => [
			'ak'       => Env::get('upload_aliyun_ak'),
			'sk'       => Env::get('upload_aliyun_sk'),
			'bucket'   => Env::get('upload_aliyun_bucket'),
			'endpoint' => 'http://oss-cn-beijing.aliyuncs.com',
		],

		// 腾讯云
		'qcloud' => [
			'ak'     => Env::get('upload_qcloud_ak'),
			'sk'     => Env::get('upload_qcloud_sk'),
			'bucket' => Env::get('upload_qcloud_bucket'),
		],
	],

	//图片上传配置
	'image'   => [
		//图片上传验证
		'validate' => [
			'size' => 1048576,
			'ext'  => 'jpg,png,gif,bmp',
		],

		'rule' => 'date',// 保存规则
	],
];
