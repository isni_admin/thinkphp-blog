<?php
/**
 * Talents come from diligence, and knowledge is gained by accumulation.
 *
 * @author: 晋<657306123@qq.com>
 * @date: 2019/8/26 23:32
 */
use think\facade\App;

return [
	'mini' => [
		'app_id'        => '',
		'secret'        => '',

		// 下面为可选项
		// 指定 API 调用返回结果的类型：array(default)/collection/object/raw/自定义类名
		'response_type' => 'array',

		'log' => [
			'level' => 'debug',
			'file'  => App::getRuntimePath().'wechat.log',
		],
	],
];
