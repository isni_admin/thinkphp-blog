$(function() {
	function showModal(options) {
		return xin.showModal(options).then(function(res) {
			console.log(res);
		});
	}

	$('#modal-default-btn').click(function() {
		showModal({
			title: '温馨提示',
			content: 'hello world!',
		});
	});

	$('#modal-untitle-btn').click(function() {
		showModal({
			content: 'hello world!'
		});
	});

	$('#modal-confirm-btn').click(function() {
		showModal({
			content: 'hello world!',
			showCancel: true
		});
	});

	$('#modal-richtext-btn').click(function() {
		showModal({
			content: `<h1>hello world</h1>`,
			showCancel: true
		});
	});

	$('#modal-lg-btn').click(function() {
		showModal({
			content: `<h1>hello world</h1>`,
			area: [
				'80%',
				'80%',
			]
		});
	});

	$('#modal-custom-btn').click(function() {
		showModal({
			content: '亲，你还差5元就可以凑齐10000积分了，是否继续选购？',
			showCancel: true,
			confirmText: '是的',
			cancelText: '不了'
		})
	});

	$('#modal-icon-btn').click(function() {
		showModal({
			content: '警告提示',
			icon: 'warn'
		}).then(function() {
			return showModal({
				content: '成功提示',
				icon: 'success'
			});
		}).then(function() {
			return showModal({
				content: '错误提示',
				icon: 'error'
			});
		}).then(function() {
			return showModal({
				content: '询问提示',
				icon: 'question'
			});
		}).then(function() {
			return showModal({
				content: '未授权提示',
				icon: 'unauthorized'
			});
		});
	});

	$('#modal-child-btn').click(function() {
		xin.open({
			url: 'http://www.baidu.com'
		});
	});
});
