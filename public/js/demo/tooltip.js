$(function() {
	// Tooltips
	$('#tooltip-btn').on('mouseenter', function() {
		const self = $(this);
		const options = {
			el: this,
			time: 1000,
			tips: 3
		};
		options.content = self.data('tooltip');

		xin.tooltip(options);
	});
});
